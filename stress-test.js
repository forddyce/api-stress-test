/**
 * 
 * Determine the limits of the system
 * Purpose is to verify the reliability and stability under extreme conditions
 *
 * Run a stress test for:
 * - determine how system behave under extreme conditions
 * - determine max capacity of system in terms of number of users
 * - determine breaking point of the system and the failure mode
 * - test if system will recover without manual intervention even after the test
 *
 * this is for load test only
 * 
 */

import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  insecureSkipTLSVerify: true,
  noConnectionReuse: false,
  stages: [
    { duration: '2m', target: 100 }, // below normal load
    { duration: '5m', target: 100 },
    { duration: '2m', target: 200 }, // normal load
    { duration: '5m', target: 200 },
    { duration: '2m', target: 300 }, // around the breaking point
    { duration: '5m', target: 300 },
    { duration: '2m', target: 400 }, // beyond breaking point
    { duration: '5m', target: 400 },
    { duration: '10m', target: 0 }, // scale down, recovery stage
  ]
};

const API_URL = '';

export default () => {
  // http.get('');
  // http.batch([
  //  ['GET', ''],
  //  ['GET', ''],
  //  ['GET', []]
  // ]);
  // sleep(1);
};