/**
 * 
 * To test the system reliability over a long period of time.
 * 
 * Run a soak test for:
 * - verify system does not suffer from bugs or memory leak, which will result in crash and restarts after a certain amount of time
 * - verify that expected system restarts does not lose requests
 * - find bugs related to race-conditions that appear sporadically
 * - make sure database does not exhaust the allotted storage space and just stops
 * - make sure logs don't exhaust the allotted storage space
 * - make sure external service that you depend on don't stop working after certain amount of requests are exceeded
 *
 * How to run a soak test:
 * - determine max amount of users your system can handle
 * - set VU to 75-80% of that amount
 * - run test in 3 stages: rump up to that VU -> stay there for 4-12 hours -> rump down to 0
 * 
 */

import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  insecureSkipTLSVerify: true,
  noConnectionReuse: false,
  stages: [
    { duration: '2m', target: 400 }, // ramp up to 400 users
    { duration: '3h56m', target: 400 }, // stay there for 4-ish hours
    { duration: '2m', target: 0 }, // scale down, optional
  ]
};

const API_URL = '';

export default () => {
  // http.get('');
  // http.batch([
  //  ['GET', ''],
  //  ['GET', ''],
  //  ['GET', []]
  // ]);
  // sleep(1);
};