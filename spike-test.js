/**
 * 
 * Like stress-test, but to test sudden increase in traffic (not gradually)
 * Purpose is to verify the reliability and stability under extreme conditions
 *
 * Run a spike test for:
 * - determine how system behave under sudden surge in traffic
 * - determine if system will recover after traffic has subsided
 *
 * Possible outcome:
 * - Excellent: system does not degrade during and after the surge. Response time is similar during low and high traffic
 * - Good: response time is slower, but system does not produce any error. All requests are handled
 * - Poor: error when traffic surge, but recover to normal after traffic subsided
 * - Bad: crash after traffic surge, system does not recover without manual intervention
 * 
 */

import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  insecureSkipTLSVerify: true,
  noConnectionReuse: false,
  stages: [
    { duration: '10s', target: 100 }, // below normal load
    { duration: '1m', target: 100 },
    { duration: '10s', target: 1400 }, // spike to 1400 users
    { duration: '3m', target: 1400 }, // stay at 1400 users for 3 minutes
    { duration: '10s', target: 100 }, // scale down, recovery stage
    { duration: '3m', target: 100 },
    { duration: '10s', target: 0 },
  ]
};

const API_URL = '';

export default () => {
  // http.get('');
  // http.batch([
  //  ['GET', ''],
  //  ['GET', ''],
  //  ['GET', []]
  // ]);
  // sleep(1);
};