/**
 * 
 * How many concurrent user and requests that the system can handle, per second.
 * This is a test not meant to break the system, but to test the system under daily performance.
 * 
 * Run a load test to:
 * - assess current performance under typical or peak load
 * - make sure API performance is still ok even after updating the system
 *
 * Can be used to simulate normal days in business.
 * 
 */

import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  insecureSkipTLSVerify: true,
  noConnectionReuse: false,
  stages: [
    { duration: '5m', target: 100 }, // simulate ramp up traffic from 1 to 100 over 5 minutes
    { duration: '10m', target: 100 }, // stay at 100 users for 10 minutes
    { duration: '5m', target: 0 }, // ramp down to 0 users
  ],
  thresholds: {
    http_req_duration: ['p(99)<150'], // 99% of requests must complete below 150ms
  }
};

const API_URL = '';

export default () => {
  // http.get('');
  // http.batch([
  //  ['GET', ''],
  //  ['GET', ''],
  //  ['GET', []]
  // ]);
  // sleep(1);
};