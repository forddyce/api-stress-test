# Stress Test API Endpoint

`https://bitbucket.org/forddyce/api-stress-test/src/master/`

Install k6 (https://k6.io/docs/getting-started/installation/)

Edit the API routes that need to test in the js file.

Run individual test with `k6 run {filename}.js`